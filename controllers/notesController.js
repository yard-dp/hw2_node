const Note = require('../models/noteModel');
const asyncHandler = require('express-async-handler');

const getNotes = asyncHandler(async (req, res)=> {
  console.info(`${req.method} ${req.originalUrl}`);
  const notes = await Note.find({userId: req.user._id});
  if (notes) {
    res.json({notes: notes});
  } else {
    res.status(400).json({message: 'No notes'});
  }
});

const createNote = asyncHandler(async (req, res) => {
  console.info(`${req.method} ${req.originalUrl}`);
  const {text} = req.body;
  if (!text) {
    res.status(400).json({message: 'No text'});
    throw new Error('Please fill the field');
  } else {
    const note = new Note({userId: req.user._id, completed: false, text: text});
    await note.save();

    res.status(200).json({message: 'Success'});
  }
});

const getNoteById = asyncHandler(async (req, res)=> {
  console.info(`${req.method} ${req.originalUrl}`);
  const note = await Note.findById(req.params.id);

  if (note) {
    res.status(200).json({note: {_id: req.params.id, userId: note.userId,
      completed: note.completed, text: note.text,
      createdDate: note.createdDate}});
  } else {
    res.status(400).json({message: 'Note Not Found!'});
  }
});

const checkNote = asyncHandler(async (req, res) => {
  console.info(`${req.method} ${req.originalUrl}`);
  const note = await Note.findById(req.params.id);

  if (note.userId.toString() !== req.user._id.toString()) {
    res.status(400).json({message: 'Could not check/uncheck Note'});
    throw new Error('You can\'t perform this action');
  }
  if (note) {
    note.completed = !note.completed;
    await note.save();
    res.status(200).json({message: 'Success'});
  } else {
    res.status(500).json({message: 'Server Error'});
    throw new Error('Note not found');
  }
});
const updateNote = asyncHandler(async (req, res) => {
  console.info(`${req.method} ${req.originalUrl}`);
  const {text} = req.body;

  const note = await Note.findById(req.params.id);

  if (note.userId.toString() !== req.user._id.toString()) {
    res.status(400).json({message: 'Could not update Note'});
    throw new Error('You can\'t perform this action');
  }

  if (note) {
    note.text = text;
    await note.save();
    res.status(200).json({message: 'Success'});
  } else {
    res.status(500).json({message: 'Server Error'});
    throw new Error('Note not found');
  }
});

const deleteNote = asyncHandler(async (req, res) => {
  console.info(`${req.method} ${req.originalUrl}`);
  const note = await Note.findById(req.params.id);

  if (note.userId.toString() !== req.user._id.toString()) {
    res.status(400).json({message: 'Could not delete Note'});
    throw new Error('You can\'t perform this action');
  }

  if (note) {
    await note.remove();
    res.status(200).json({message: 'Success'});
  } else {
    res.status(500).json({message: 'Server Error'});
    throw new Error('Note not Found');
  }
});
module.exports = {getNotes, createNote, getNoteById,
  updateNote, deleteNote, checkNote};
