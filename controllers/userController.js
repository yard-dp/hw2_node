const asyncHandler = require('express-async-handler');
const User = require('../models/userModel');

const getProfileInfo = asyncHandler(async (req, res)=> {
  console.info(`${req.method} ${req.originalUrl}`);
  const myUser = await User.findById(req.user._id);
  if (myUser) {
    res.status(200).json({user: {_id: req.user._id, username: myUser.username,
      createdDate: myUser.createdDate}});
  } else {
    res.status(400).json({message: 'No such user'});
  }
});
const changeProfilePassword = asyncHandler(async (req, res)=> {
  console.info(`${req.method} ${req.originalUrl}`);
  const myUser = await User.findById(req.user._id);
  const newPassword = req.body.newPassword;
  if (myUser) {
    myUser.password = newPassword;
    await myUser.save();
    res.status(200).json({message: 'Success'});
  } else {
    res.status(400).json({message: 'Failed to update password!'});
  }
});
const deleteUser = asyncHandler(async (req, res) => {
  console.info(`${req.method} ${req.originalUrl}`);
  const myUser = await User.findById(req.user._id);
  if (myUser) {
    await myUser.remove();
    res.status(200).json({message: 'Success'});
  } else {
    res.status(400).json({message: 'Could not delete User\'s info'});
  }
});

module.exports = {getProfileInfo, changeProfilePassword, deleteUser};
