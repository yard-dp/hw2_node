const asyncHandler = require('express-async-handler');
const User = require('../models/userModel');
const generateToken = require('../utils/generateToken');

const registerUser = asyncHandler(async (req, res) => {
  console.info(`${req.method} ${req.originalUrl}`);
  const {username, password} = req.body;

  const userExists = await User.findOne({username});

  if (userExists) {
    res.status(400).json({message: 'Error: User Already Exists'});
    throw new Error('User already Exists');
  }
  const user = await User.create({
    username,
    password,
  });
  if (user) {
    res.status(200).json({message: 'Success'});
  } else {
    res.status(500).json({message: 'Server Error'});
  }
});

const loginUser = asyncHandler(async (req, res) => {
  console.info(`${req.method} ${req.originalUrl}`);
  const {username, password} = req.body;

  const user = await User.findOne({username});

  if (user && ( await user.matchPassword(password))) {
    res.status(200).json({message: 'Success',
      jwt_token: generateToken(user._id)});
  } else {
    res.status(400).json({message: 'Invalid email or password'});
    throw new Error('Invalid email or password');
  }
});
module.exports = {registerUser, loginUser};
