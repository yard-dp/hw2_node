const express = require('express');
const dotenv = require('dotenv');
const connectDB = require('./config/db');
const authRoutes = require('./routes/AuthRoutes');
const notesRoutes = require('./routes/NotesRoutes');
const userRoutes = require('./routes/userRoutes');
const app = express();
dotenv.config();
connectDB();

app.use(express.json());
app.get('/', (req, res)=> {
  res.send('API is running');
});

app.use('/api/auth', authRoutes);
app.use('/api/notes', notesRoutes);
app.use('/api/users', userRoutes);
const PORT = process.env.PORT || 8080;

app.listen(PORT, console.log(`Server started on Port ${PORT}`));
