const express = require('express');
const {getNotes, createNote, getNoteById, updateNote,
  deleteNote, checkNote} = require('../controllers/notesController');
const {protect} = require('../middlewares/authMiddleware');
const router = express.Router();

router.get('/', protect, getNotes);
router.post('/', protect, createNote);
router.route('/:id').get(getNoteById)
    .put(protect, updateNote)
    .delete(protect, deleteNote).patch(protect, checkNote);

module.exports = router;
