const express = require('express');
const {getProfileInfo, changeProfilePassword,
  deleteUser} = require('../controllers/userController');
const {protect} = require('../middlewares/authMiddleware');
const router = express.Router();

router.route('/me').get(protect, getProfileInfo)
    .delete(protect, deleteUser).patch(protect, changeProfilePassword);
module.exports = router;
