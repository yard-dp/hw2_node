const mongoose = require('mongoose');

const noteSchema = mongoose.Schema(
    {
      userId: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: 'User',
      },
      completed: {
        type: Boolean,
        required: true,
      },
      text: {
        type: String,
        required: true,
      },
      createdDate: {
        type: Date,
        default: Date.now(),
        required: true,
      },
    },
    {
      timestamps: true,
    },
);

const Note = mongoose.model('Note', noteSchema);

module.exports = Note;
